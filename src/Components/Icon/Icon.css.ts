import { recipe } from "@vanilla-extract/recipes";
export const icon = recipe({
  base: {},
  variants: {
    size: {
      small: {
        width: "16px !important",
        height: "16px !important",
      },
      medium: {
        width: "20px !important",
        height: "20px !important",
      },
      large: {
        width: "24px !important",
        height: "24px !important",
      },
      xlarge: {
        width: "28px !important",
        height: "28px !important",
      },
    },
  },
});
