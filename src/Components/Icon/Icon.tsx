import {
  MdArrowForward,
  MdWhatshot,
  MdOutlineWhatshot,
  MdDone,
} from "react-icons/md";
import type { IconType } from "react-icons";
import cn from "classnames";
import { SizeT } from "../../types/size";
import { icon } from "./Icon.css";

export const icons = {
  ArrowForward: MdArrowForward,
  Whatshot: MdWhatshot,
  WhatshotTwoTone: MdOutlineWhatshot,
  Done: MdDone,
};

export type IconT = keyof typeof icons;

export type IconPropT = {
  name: keyof typeof icons;
  size?: SizeT;
  className?: string;
} & React.ComponentProps<IconType>;

const Icon: React.FC<IconPropT> = ({
  name,
  size = "medium",
  className,
  ...props
}) => {
  const classNames = cn(className, icon({ size }));
  const Component = icons[name];
  return <Component className={classNames} {...props} />;
};

export default Icon;
