import { recipe } from "@vanilla-extract/recipes";
import { style } from "@vanilla-extract/css";
import { sprinkles } from "../../styles/sprinkles.css";
import { vars } from "../../styles/theme.css";

export const button = recipe({
  base: [
    {
      textDecoration: "none",
      display: "inline-flex",
      alignItems: "center",
      width: "fit-content",
      outline: "none",
      boxSizing: "border-box",
      borderWidth: 0,
      borderStyle: "solid",
      borderRadius: "24px",
      appearance: "none",
      backgroundColor: "transparent",
      ":disabled": {
        cursor: "not-allowed",
        boxShadow: "none",
        color: vars.color["grey-50"],
      },
    },
    sprinkles({
      justifyContent: "center",
      cursor: "pointer",
    }),
  ],
  variants: {
    type: {
      primary: [
        {
          backgroundColor: vars.color.primary,
          color: vars.color.white,
          selectors: {
            "&:hover:not(:disabled)": {
              backgroundColor: vars.color["blue-40"],
            },
            "&:active:not(:disabled)": {
              backgroundColor: vars.color["blue-60"],
            },
          },
          ":disabled": {
            backgroundColor: vars.color["grey-30"],
          },
        },
        sprinkles({ fontWeight: "semibold" }),
      ],
      outline: [
        {
          color: vars.color["grey-100"],
          borderColor: vars.color["grey-30"],
          borderWidth: "1px",
          selectors: {
            "&:hover:not(:disabled)": {
              backgroundColor: vars.color["grey-10"],
            },
            "&:active:not(:disabled)": {
              backgroundColor: vars.color["grey-20"],
            },
          },
          ":disabled": {
            backgroundColor: "transparent",
          },
        },
        sprinkles({ fontWeight: "medium" }),
      ],
      text: [
        {
          color: vars.color["blue-50"],
          selectors: {
            "&:hover:not(:disabled)": {
              backgroundColor: vars.color["blue-10"],
            },
            "&:active:not(:disabled)": {
              backgroundColor: vars.color["blue-20"],
            },
          },
        },
        sprinkles({ fontWeight: "medium" }),
      ],
    },
    size: {
      small: [
        {
          minWidth: "72px",
          height: "32px",
        },
        sprinkles({ paddingX: 12, paddingY: 8, fontSize: 12 }),
      ],
      medium: [
        {
          minWidth: "96px",
          height: "40px",
        },
        sprinkles({ paddingX: 16, paddingY: 8, fontSize: 14 }),
      ],
      large: [
        {
          minWidth: "128px",
          height: "48px",
        },
        sprinkles({ paddingX: 20, paddingY: 8, fontSize: 16 }),
      ],
      xlarge: [
        {
          minWidth: "160px",
          height: "56px",
          borderRadius: "28px",
        },
        sprinkles({ paddingX: 24, paddingY: 8, fontSize: 18 }),
      ],
    },
  },
});

export const icon = sprinkles({ marginLeft: 4 });
