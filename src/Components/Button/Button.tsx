import { button as buttonStyles, icon as iconStyles } from "./Button.css";

import Icon from "../Icon/Icon";
import type { IconT } from "../Icon/Icon";
import { SizeT } from "../../types/size";
export type VariantT = "primary" | "outline" | "text";

export type IconPositionT = "left" | "right";

export type ButtonT = {
  id?: string;
  variant?: VariantT;
  size?: SizeT;
  onClick?: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  disabled?: boolean;
  loading?: boolean;
  icon?: IconT;
  iconPosition?: IconPositionT;
  iconClassName?: string;
} & React.ButtonHTMLAttributes<HTMLButtonElement>;

const Button: React.FC<ButtonT> = ({
  disabled,
  variant = "primary",
  size = "medium",
  icon,
  className,
  children,

  ...props
}) => {
  return (
    <button
      className={buttonStyles({ type: variant, size })}
      disabled={disabled}
      {...props}
    >
      {children}
      {icon && <Icon name={icon} size={size} className={iconStyles} />}
    </button>
  );
};

export default Button;
