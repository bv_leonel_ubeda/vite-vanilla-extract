import { button as buttonStyles, icon as iconStyles } from "./Button.css";
import cn from "classnames";
import Icon from "../Icon/Icon";
import type { IconT } from "../Icon/Icon";
import { SizeT } from "../../types/size";
import React from "react";
export type VariantT = "primary" | "outline" | "text";
export type IconPositionT = "left" | "right";

export type Common = {
  size?: SizeT;
  loading?: boolean;
  variant?: VariantT;
  icon?: IconT;
  iconPosition?: IconPositionT;
  iconClassName?: string;
  as?: React.ReactElement | React.ElementType;
  iconProps?: Omit<React.ComponentPropsWithoutRef<typeof Icon>, "name">;
};

interface Props<C extends React.ElementType> extends Common {
  as?: C;
}

type ButtonProps<C extends React.ElementType> = Props<C> &
  Omit<React.ComponentPropsWithoutRef<C>, keyof Props<C>>;

/* 
  TODO: LEER EXPLICACION 
  https://www.benmvp.com/blog/polymorphic-react-components-typescript/
  o
  https://react-typescript-cheatsheet.netlify.app/docs/advanced/patterns_by_usecase/
*/

export const Button = <C extends React.ElementType = "button">({
  as,
  icon,
  size = "medium",
  variant = "primary",
  children,
  iconProps,
  ...props
}: ButtonProps<C>) => {
  const Component = as || "button";
  const classNames = cn(props.className, buttonStyles({ type: variant, size }));
  return (
    <Component {...props} className={classNames}>
      {children}
      {icon && (
        <Icon name={icon} size={size} className={iconStyles} {...iconProps} />
      )}
    </Component>
  );
};

export default Button;
