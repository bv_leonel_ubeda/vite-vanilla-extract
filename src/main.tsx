import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { themeClass } from "./styles/theme.css";

ReactDOM.render(
  <React.StrictMode>
    <div className={themeClass}>
      <App />
    </div>
  </React.StrictMode>,
  document.getElementById("root")
);
