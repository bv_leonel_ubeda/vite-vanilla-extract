import { createTheme } from "@vanilla-extract/css";
import space from "./vars/space";

export const responsive = {
  sm: "576px",
  md: "768px",
  lg: "992px",
  xl: "1200px",
  "2xl": "1360px",
};

export const [themeClass, vars] = createTheme({
  space: space,
  color: {
    brand: "blue",
    "danger-50": "#EB5454",
    "blue-10": "#F1F6FE",
    "blue-20": "#CFDFFC",
    "blue-40": "#3A74F8",
    "blue-50": "#1F61F7",
    "blue-60": "#204FCC",
    "blue-80": "#0C2B8A",

    //
    "grey-10": "#F5F7FA",
    "grey-20": "#EBEFF5",
    "grey-30": "#DDE3ED",
    "grey-40": "#C8D1E0",
    "grey-50": "#AFBACC",
    "grey-80": "#58606E",
    "grey-90": "#434A54",
    "grey-100": "#060B25",
    primary: "#1F61F7",
    white: "white",
  },
});
