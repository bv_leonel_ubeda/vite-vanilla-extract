import { defineProperties, createSprinkles } from "@vanilla-extract/sprinkles";
import { vars } from "./theme.css";

const borderWidth = {
  1: "0.0625rem",
  2: "0.125rem",
  3: "0.1875rem",
};

const colors = {
  bg: vars.color.white,
  "bg-10": vars.color["grey-10"],
  disabled: vars.color["grey-20"],
  border: vars.color["grey-30"],
  paragraph: vars.color["grey-80"],
  title: vars.color["grey-100"],
  "icon-dark": vars.color["grey-100"],
  placeholder: vars.color["grey-50"],
  white: "white",
};

const responsiveProperties = defineProperties({
  conditions: {
    mobile: {},
    tablet: { "@media": "screen and (min-width: 768px)" },
    desktop: { "@media": "screen and (min-width: 1024px)" },
  },
  defaultCondition: "mobile",
  properties: {
    fontWeight: {
      normal: 400,
      medium: 500,
      semibold: 600,
      bold: 700,
      black: 800,
    },
    borderRadius: {
      small: "8px",
      medium: "12px",
      large: "24px",
    },
    boxShadow: {
      small: "0px 4px 12px rgba(18, 25, 84, 0.07)",
      medium: "0px 6px 14px rgba(18, 25, 84, 0.1)",
      large: "0px 10px 16px rgba(18, 25, 84, 0.07)",
      xlarge: "0px 16px 24px rgba(18, 25, 84, 0.07)",
    },
    cursor: ["pointer"],
    display: ["none", "flex", "block", "inline"],
    flexDirection: ["row", "column"],
    justifyContent: [
      "stretch",
      "flex-start",
      "center",
      "flex-end",
      "space-around",
      "space-between",
    ],
    alignItems: ["stretch", "flex-start", "center", "flex-end"],
    paddingTop: vars.space,
    paddingBottom: vars.space,
    paddingLeft: vars.space,
    paddingRight: vars.space,
    marginTop: vars.space,
    marginBottom: vars.space,
    marginLeft: vars.space,
    marginRight: vars.space,

    borderWidth: vars.space,
    borderTop: vars.space,
    borderBottom: vars.space,
    borderLeft: vars.space,
    borderRight: vars.space,
    borderColor: colors,
    height: vars.space,
    width: vars.space,
    fontSize: {
      12: {
        fontSize: "12px",
        lineHeight: "16px",
      },
      14: {
        fontSize: "14px",
        lineHeight: "24px",
      },
      16: {
        fontSize: "16px",
        lineHeight: "24px",
      },
      18: {
        fontSize: "18px",
        lineHeight: "24px",
      },
      24: {
        fontSize: "24px",
        lineHeight: "32px",
      },
      32: {
        fontSize: "32px",
        lineHeight: "40px",
      },
      40: {
        fontSize: "40px",
        lineHeight: "48px",
      },
      28: {
        fontSize: "48px",
        lineHeight: "56px",
      },
    },
    // etc.
  },
  shorthands: {
    padding: ["paddingTop", "paddingBottom", "paddingLeft", "paddingRight"],
    paddingX: ["paddingLeft", "paddingRight"],
    paddingY: ["paddingTop", "paddingBottom"],
    marginX: ["marginLeft", "marginRight"],
    marginY: ["marginTop", "marginBottom"],
    placeItems: ["justifyContent", "alignItems"],
  },
});

const colorProperties = defineProperties({
  properties: {
    color: colors,
    background: colors,
  },
});

export const sprinkles = createSprinkles(responsiveProperties, colorProperties);

// It's a good idea to export the Sprinkles type too
export type Sprinkles = Parameters<typeof sprinkles>[0];
