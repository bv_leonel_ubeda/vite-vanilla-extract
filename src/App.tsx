import { useState } from "react";
import PolymorphicButton from "./Components/Button/PolymorphicButton";
import Button, { VariantT } from "./Components/Button/Button";
import { SizeT } from "./types/size";
import Icon, { icons } from "./Components/Icon/Icon";
import { sprinkles } from "./styles/sprinkles.css";

const sizes: Array<SizeT> = ["small", "medium", "large", "xlarge"];
const variants: Array<VariantT> = ["primary", "outline", "text"];

const iconNames = Object.keys(icons);

function App() {
  const [size, setSize] = useState<SizeT>("medium");
  const [variant, setVariant] = useState<VariantT>("primary");
  const [disabled, setDisabled] = useState(false);

  const [iconName, setIconName] = useState<keyof typeof icons>("ArrowForward");
  return (
    <div>
      <label htmlFor="disable">Disable</label>
      <input
        type="checkbox"
        id="disable"
        checked={disabled}
        onClick={() => setDisabled((state) => !state)}
      />
      <div>
        {sizes.map((e) => (
          <PolymorphicButton
            style={{ margin: "0 10px" }}
            onClick={() => setSize(e)}
            // Esto es un problema, no puedo hacerle override al background color,
            // tiene mas prioridad los estilos default del boton y variante
            className={sprinkles({ background: "icon-dark" })}
            variant="outline"
            size="small"
          >
            {e}
          </PolymorphicButton>
        ))}
      </div>

      <div style={{ margin: "10px 0" }}>
        {variants.map((e) => (
          <PolymorphicButton
            style={{ margin: "0 10px" }}
            onClick={() => setVariant(e)}
            variant="outline"
            size="small"
          >
            {e}
          </PolymorphicButton>
        ))}
      </div>
      <hr />
      <div
        style={{
          display: "flex",
        }}
      >
        <Button
          size={size}
          variant={variant}
          disabled={disabled}
          style={{ margin: "0 10px" }}
        >
          Default
        </Button>

        <PolymorphicButton
          as="a"
          href="https://www.google.com/"
          target="_blank"
          size={size}
          variant={variant}
          style={{ margin: "0 10px" }}
        >
          Anchor element
        </PolymorphicButton>

        <PolymorphicButton
          size={size}
          variant={variant}
          href="https://www.google.com/"
          as="a"
          style={{ margin: "0 10px" }}
          icon="ArrowForward"
        >
          Div element
        </PolymorphicButton>
      </div>
      <hr style={{ marginTop: "40px" }} />
      <div>
        {iconNames.map((e: any) => (
          <PolymorphicButton
            onClick={() => setIconName(e)}
            variant="outline"
            size="small"
          >
            {e}
          </PolymorphicButton>
        ))}
      </div>
      <hr />
      <Icon name={iconName} size="xlarge" />
    </div>
  );
}

export default App;
